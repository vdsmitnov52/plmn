#!/usr/bin/python -u
# coding: koi8-r
import Tkinter
import tkFileDialog
import ScrolledText
def wwf(key):
	u"""������ � �������"""
	# �������� ����� - ������� ����� � text
	if key == "open":
		tkfd = tkFileDialog.askopenfile
		try:
			with tkfd(title=u"�������� ���� ��� ��������", initialdir="c:\\", filetypes=(("text file", "*.txt"),))as file_name:
				text.name = file_name.name	#str(file_name.name)
				for line in enumerate(file_name):
					text.insert("%s.0" % (line[0] + 1), line[1].decode("koi8-r"))
		except AttributeError:
			print u"���� ��� �������� �� ������"
	# ������� ����� - ������� ����� ������� text
	if key == "close":
		text.delete("1.0", text.index("end"))
	# ���������� ����� - ���������� ����� �� text � ����
	if key == "save":
		with open(text.name, "w") as file_name:
			text_to_file = text.get("1.0", text.index("end"))
			file_name.write(text_to_file.encode("koi8-r"))
	# ���������� ����� - ���������� ����� �� text � ����
	if key == "save as":
		tkfd = tkFileDialog.asksaveasfile
		try:
			with tkfd(title=u"�������� ���� ��� ��������", initialfile = text.name, initialdir="c:\\", filetypes=(("text file", "*.txt"),))as file_name:
				text_to_file = text.get("1.0", text.index("end"))
				file_name.write(text_to_file.encode("koi8-r"))
		except AttributeError:
			print u"���� ��� ���������� �� ������"
	# ����� �� ���������
	if key == "exit":
		root.destroy()
def call_cmenu(event):
	u"""����������� ������������ ����."""
	pos_x = text.winfo_rootx() + event.x
	pos_y = text.winfo_rooty() + event.y
	cmenu.tk_popup(pos_x, pos_y)
def copy():
	u"""����������� ���������� ������ � ����� ������."""
	try:
		text_to_clipboard = text.get("sel.first", "sel.last")
	except Tkinter.TclError:
		print u"���������� ������� ���"
		text_to_clipboard = u""
	root.clipboard_clear()
	root.clipboard_append(text_to_clipboard)
def paste():
	u"""������� ������ �� ������ ������."""
	try:
		text.insert("current", root.clipboard_get())
	except Tkinter.TclError:
		print u"����� ������ ����"

def	keys (event):
	ks = ['char', 'x', 'y', 'keycode', 'keysym', 'keysym_num', 'widget']
	for k in ks:	print "%12s: %s" % (k, repr(getattr(event, k)))
#	for k in dir(event):	print "%12s: %s" % (k, repr(getattr(event, k)))

root = Tkinter.Tk()
root.focus_force()
menu = Tkinter.Menu(root)
root.config(menu=menu)
menu_file = Tkinter.Menu(menu, tearoff=False)
menu_file.add_command(label=u"�������", command=lambda: wwf("open"))
menu_file.add_command(label=u"�������", command=lambda: wwf("close"))
menu_file.add_command(label=u"���������", command=lambda: wwf("save"))
menu_file.add_command(label=u"��������� ���", command=lambda: wwf("save as"))
menu_file.add_command(label=u"�����", command=lambda: wwf("exit"))
menu.add_cascade(label=u"������ � �������", menu=menu_file)
cmenu = Tkinter.Menu(root, tearoff=False)
cmenu.add_command(label=u"����������", command=copy)
cmenu.add_command(label=u"��������", command=paste)
text = ScrolledText.ScrolledText(root, width=100, height=40, font=14)
text.name = u"c:\\������.txt"
text.bind("<ButtonRelease-3>", call_cmenu)
text.bind("<KeyPress>", keys)
text.pack()
root.mainloop()
