#!/usr/bin/python -u
# -*- coding: koi8-r -*-

import	os, sys, time
import	urllib	#, urllib2
from	Tkinter	import *
from	ttk	import *
import	tkMessageBox

def	get_in_zterm (curs, MCC, MNC, lac, cid):
	query = "SELECT * FROM plmn_place WHERE cod_MCC=%d AND cod_MNC=%d AND cell_id=%d AND lac=%d;" % (country, oper, cid, lac)
	curs.execute(query)
	row = curs.fetchone()
	bsrow = {}
	if row:
		fields = [f[0] for f in curs.description]
		for key in fields:
			bsrow [key] = row[fields.index (key)]
		print "<div class='reslt'><br />"
	#	if bsrow ['longitude'] != '' and bsrow ['latitude'] != '':
		if bsrow ['longitude'] and bsrow ['latitude']:
			print "<table class='fs12'>"
			print """<tr><td align='right' nowrap>���������� ��:</td><td> <b>%s</b> <b>%s</b> (������, �������).</td></tr>""" % (
					bsrow ['latitude'], bsrow ['longitude'])
	#		if bsrow['rem']:
			pkey = "cod_MCC=%d AND cod_MNC=%d AND cell_id=%d AND lac=%d" % (
					bsrow['cod_MCC'], bsrow['cod_MNC'], bsrow['cell_id'], bsrow['lac'])
			print """<tr valign='top'><td align='right'>����������:</td><td>
			<input name='new_rem' type='text' size=48 value='%s' /><br />
			<input name='where' type='hidden' value='%s' />
			<input type='button' onclick="document.myForm.stat.value='set_rem'; document.myForm.submit()" value='��������' />
			</td></tr>""" % (bsrow['rem'], pkey)
			print "</table>"
	#		print fields, bsrow
		else:	print "���������� �� � �� <b>�����������!</b>"
		print "<br />&nbsp;</div>"	#, bsrow
		return 1
	else:
		print "��� ������ � �� <b>zterm</b> !<br />&nbsp;</div>"

def get_bs_google (MCC, MNC, cid, lac):
	""" ������ ��������� �� � http://www.google.com/glm/mmap 
	"""
	net = MCC*100+MNC
	a = '000E00000000000000000000000000001B0000000000000000000000030000'
	b = hex(cid)[2:].zfill(8) + hex(lac)[2:].zfill(8)
	c = hex(divmod(net,100)[1])[2:].zfill(8) + hex(divmod(net,100)[0])[2:].zfill(8)
	string = (a + b + c + 'FFFFFFFF00000000').decode('hex')
	result = {}
	try:
		data = urllib.urlopen('http://www.google.com/glm/mmap',string)
		r = data.read().encode('hex')
#		print r
		if len(r) > 14:
			lat = float(int(r[14:22],16))/1000000
			lon = float(int(r[22:30],16))/1000000
			if (lat > 360):		lat = lat - 2**32/1000000 
			if (lon > 360):		lon = lon - 2**32/1000000 
			result ['lat'] = lat
			result ['lon'] = lon
			result ['radius'] = int(r[30:38],16) 
			result ['valid'] = int(r[38:46],16)
			return result
		else:
			print "Google - ��� ������!<br />", MCC, MNC, lac, cid
	except:
		print "Google - ��� ������!<br />", MCC, MNC, lac, cid

def get_bs_yandex (MCC, MNC, cid, lac):
	""" ������ ��������� �� � http://mobile.maps.yandex.net/cellid_location/ 
	"""
	url = 'http://mobile.maps.yandex.net/cellid_location/?&cellid=%d&operatorid=%d&countrycode=%d&lac=%d' % (cid, MNC, MCC, lac)
#	print url
	result = {}
	try:
		data = urllib.urlopen(url)
		r = data.read()
		if r.find('Not found') < 0:
			i0 = r.find( ' latitude="')
			i1 = r.index( '" longitude="')
			result ['lat'] = r[i0+11:i0+21]
			result ['lon'] = r[i1+13:i1+23]
			return result
	except:
		print "Yandex - ��� ������!<br />", MCC, MNC, lac, cid

'''
def get_bs_mozilla (MCC, MNC, cid, lac):
	dddd = {"radio": "gsm",
		"cell": [ {
		"radio": "umts",
		"mcc": 123,
		"mnc": 123,
		"lac": 12345,
		"cid": 12345,
		"signal": -61,
		"asu": 26
		}],}
	dddd['cell'][0]['mcc'] = MCC
	dddd['cell'][0]['mnc'] = MNC
	dddd['cell'][0]['lac'] = lac
	dddd['cell'][0]['cid'] = cid
	print str (dddd).replace("'", '"')
	url = r'https://location.services.mozilla.com/v1/submit?key=Search'	# % str (dddd)
	print url
	data = urllib.urlopen(url, str (dddd).replace("'", '"'))
	r = data.read()
	print r
'''
def search (mark):
	print mark
	global	smcc, smnc, slac, scid
	global	messgs
	flis_err = 0

	ssmcc = smcc.get()
	if not ssmcc.isdigit():
		tkMessageBox.showwarning("Warning", u"Data format error.", detail=u'MCC is digital!')
		flis_err = 1
	ssmnc = smnc.get()
	if not ssmnc.isdigit():
		tkMessageBox.showwarning("Warning", u"Data format error.", detail=u'MNC is digital!')
		flis_err = 1
	sslac = slac.get()
	sscid = scid.get()
	try:
		lac = int (sslac, 16)
		slac.delete(0, END)
		slac.insert(0, '%04X' % lac)
		cid = int (sscid, 16)
		scid.delete(0, END)
		scid.insert(0, '%04X' % cid)
	except ValueError:
		tkMessageBox.showwarning("Warning", u"Data format error.", detail=u'LAC and CID is digital in HEX format!')
		flis_err = 1

	if flis_err:
		messgs.insert(1.0, u"Data format error.")
		return
	mcc = int (ssmcc)
	mnc = int (ssmnc)
	messgs.delete(1.0, END)
	messgs.insert(1.0, "Seach:\t %d, %d, %04x, %04x\n" % (mcc, mnc, lac, cid))
	res = get_bs_google (mcc, mnc, cid, lac)
	if res:
		messgs.insert(2.0, "Google:\t %s   %s\n" % (str (res['lat']), str (res['lon'])))
	else:	messgs.insert(2.0, "Google:\t BSS not found.\n")
	res = get_bs_yandex (mcc, mnc, cid, lac)
	if res:
		messgs.insert(3.0, "Yandex:\t %s  %s\n" % (str (res['lat']), str (res['lon'])))
	else:	messgs.insert(3.0, "Yandex:\t BSS not found.\n")
	

if __name__ == '__main__':
	'''
	MCC =	250
	MNC =	1
	lac =	0x0af0	# 0x0152	# 0x3627
	cid =	0x0018	# 0x4e62	# 0x0af0
#	print	get_bs_google (MCC, MNC, cid, lac)
	print	get_bs_yandex (MCC, MNC, cid, lac)
	os.exit(0)
#	print	get_bs_mozilla (MCC, MNC, cid, lac)
	'''
	root = Tk()
	win = Frame(root)
	root.title ('Search BS')
	root.geometry('420x300')
	body = Frame(root)
	body.grid()
	j = 1
	Label(body, text="MCC:").grid(column=0, row=j, sticky=E, padx=2,pady=4)
	Label(body, text=u"Mobile Country Code (��� ������)").grid(column=3, row=j, sticky=W, padx=2,pady=4)
	smcc = Entry(body, width=4)
	smcc.grid(column=1, row=j, sticky=W)
	smcc.insert(1, '250')
	j += 1
	Label(body, text="MNC:").grid(column=0, row=j, sticky=E, padx=2,pady=4)
	Label(body, text=u"Mobile Network Code (��� ����)").grid(column=3, row=j, sticky=W, padx=2,pady=4)
	smnc = Entry(body, width=4)
	smnc.grid(column=1, row=j, sticky=W)
	j += 1
	Label(body, text="LAC:").grid(column=0, row=j, sticky=E, padx=2,pady=4)
	Label(body, text=u"Location Area Code (��� ���������)").grid(column=3, row=j, sticky=W, padx=2,pady=4)
	Label(body, text=u"HEX").grid(column=4, row=j, sticky=W, padx=2,pady=4)
	slac = Entry(body, width=8)
	slac.grid(column=1, row=j, sticky=W)
	j += 1
	Label(body, text="CID:").grid(column=0, row=j, sticky=E, padx=2,pady=4)
	Label(body, text=u"Cell ID (������������� ����)").grid(column=3, row=j, sticky=W, padx=2,pady=4)
	Label(body, text=u"HEX").grid(column=4, row=j, sticky=W, padx=2,pady=4)
	scid = Entry(body, width=8)
	scid.grid(column=1, row=j, sticky=W)
	j += 1
	bsearch = Button(body, text='Search', width=8, command=(lambda: search('xml')))
	bexit = Button(body, text='Exit', width=5, command=(lambda: root.destroy()))
	bsearch.grid(column=0, row=j, columnspan=2, sticky=E, padx=1,pady=4)
	bexit.grid(column=3, row=j, sticky=W, padx=4,pady=4)
	j += 1

	body.pack()
	fff = Frame(root)
	messgs = Text(fff, width=44, height=6, font='Areal 10', bg='#e0e0e0')
	messgs.pack()
	fff.pack()

	root.mainloop()
